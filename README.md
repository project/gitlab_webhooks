GitLab Webhooks
===================

Defines services resources so Drupal can parse and direct GitLab webhook events.

API
---

See the gitlab_webhooks.api.php for details.